import { link } from "@wordpress/icons"
import { ToolbarGroup, ToolbarButton, Popover, Button, PanelBody, PanelRow } from "@wordpress/components"
import { RichText, BlockControls, InnerBlocks, InspectorControls, MediaUpload, useBlockProps, MediaUploadCheck, __experimentalLinkControl as LinkControl } from "@wordpress/block-editor"
import { registerBlockType } from "@wordpress/blocks"
import { useState } from "@wordpress/element" //Alias of React

registerBlockType("pp-theme/pp-button",{
    apiVersion: 2,
    title: "PP Button",
    category: 'theme',
    icon: 'button',
    supports: {
        spacing: {
          margin: true,  // Enable margin UI control.
          padding: true, // Enable padding UI control.
          blockGap: false,  // Enables block spacing UI control for blocks that also use `layout`.
        },
        dimenstions: true
    },
    attributes: {
        linkObject: { 
            type: "object", 
            default: { url: "" } 
        }
    },
    edit: EditComponent,
    save: SaveComponent
}) 

function EditComponent(props) {

    const blockProps = useBlockProps( {
        className: 'pp__box pp__button',
    } );

    const PP_TEMPLATE = [
        [ 'pp-theme/pp-heading-plain', { placeholder: 'Encabezado' } ],
        [ 'pp-theme/pp-paragraph-plain', { placeholder: 'Texto' } ],
    ];

    const [isLinkPickerVisible, setIsLinkPickerVisible] = useState(false)
  
    function buttonHandler() {
      setIsLinkPickerVisible(prev => !prev)
    }
  
    function handleLinkChange(newLink) {
      props.setAttributes({ linkObject: newLink })
    }

    return (
        <>
        <div { ...blockProps }>
            <BlockControls>
                <ToolbarGroup>
                    <ToolbarButton onClick={buttonHandler} icon={link} />
                </ToolbarGroup>
            </BlockControls>

            <InnerBlocks
                template = { PP_TEMPLATE }
                allowedBlocks={["core/paragraph","core/image","outermost/icon-block"]}
            />
            {isLinkPickerVisible && (
                <Popover position="middle center">
                <LinkControl settings={[]} value={props.attributes.linkObject} onChange={handleLinkChange} />
                <Button variant="primary" onClick={() => setIsLinkPickerVisible(false)} style={{ display: "block", width: "100%" }}>
                    Confirm Link
                </Button>
                </Popover>
            )}
        </div>
        </>
    )
}

function SaveComponent(props) {
    return <InnerBlocks.Content /> 
}