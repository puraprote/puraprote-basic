import { ToolbarGroup, ToolbarButton } from "@wordpress/components"
import { RichText, InspectorControls, BlockControls } from "@wordpress/block-editor"

import { registerBlockType } from "@wordpress/blocks"

registerBlockType("pp-theme/pp-heading",{
    title: "PP H1 Heading",
    icon: 'heading',
    supports: {
        color: {
            text: true,
            background: false,
            link: true
        }
    },
    attributes: {
        text: {type: "string"},
        size: {type: "string", default: "large"},
        colorName: {type: "string", default: "blue"}
    },
    edit: EditComponent,
    save: SaveComponent
}) 

function EditComponent(props) {

    function handleTextChange(x) {
        props.setAttributes({text: x})
    }


    return (
        <>
            <InspectorControls>
            </InspectorControls>
            <RichText 
                allowedFormats={["core/bold","core/italic"]} 
                tagName="h1" 
                className={`pp__heading has-xx-large-font-size`} 
                value={props.attributes.text} onChange={handleTextChange} 
                placeholder={ 'Heading...' }
            />
        </>
    )
}

function SaveComponent(props) {
    return null
}