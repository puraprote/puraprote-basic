import { Button, PanelBody, PanelRow, ColorPalette } from "@wordpress/components"
import { RichText, InnerBlocks, InspectorControls, MediaUpload, MediaUploadCheck, useBlockProps, useSetting } from "@wordpress/block-editor"
import { registerBlockType } from "@wordpress/blocks"
import { useEffect } from "@wordpress/element" //React


//SRC: https://wordpress.stackexchange.com/questions/393524/how-to-add-template-colors-to-custom-block-options-in-wordpress-gutenberg-editor

registerBlockType("pp-theme/pp-hero",{
    title: "PP Hero",
    supports: {
      align: ["full"],
      spacing: {
        margin: true,  // Enable margin UI control.
        padding: true, // Enable padding UI control.
        blockGap: true,  // Enables block spacing UI control for blocks that also use `layout`.
      }
    },
    icon: 'cover-image',
    attributes: {
      align: { type: "string", default: "full" },
      mobileImgID: { type: "number" },
      mobileImgURL: { type: "string" },
      imgID: { type: "number" },
      imgURL: { type: "string", default: "https://fakeimg.pl/1200x400/EFEEED/eee/?text=Imagen" },
      cintillo: { type: "string" },
      cintilloBackgroundColor: { type: "string" }
    },
    edit: EditComponent,
    save: SaveComponent
}) 

function EditComponent(props) {

  const blockProps = useBlockProps( {
    className: 'pp__hero',
  } );

  const PP_TEMPLATE = [
    [ 'pp-theme/pp-heading', { placeholder: 'Título de la página' } ],
    [ 'core/paragraph', { placeholder: 'Texto' } ],
  ];

  function onFileSelect(x, whichImg = 'desktop') {
    // Log to console so we can see what WP gives us
    console.log(whichImg)
    console.log(x)
    console.log(x.sizes.full.url)
  
    // WP doesn't give us custom image sizes, so we'll have to grab it based on ID

    if ( whichImg == 'mobile' ) {
      props.setAttributes({ mobileImgID: x.id })
      props.setAttributes({ mobileImgURL: x.sizes.full.url })
    } else {
      props.setAttributes({ imgID: x.id })
      props.setAttributes({ imgURL: x.sizes.full.url })
    }

    
  }

  function handleCintilloChange(x) {
    props.setAttributes({cintillo: x})
  }






  return (
    <>
    <div { ...blockProps }>
      <InspectorControls>
        <PanelBody title="Background" initialOpen={true}>
          <PanelRow>
            <MediaUploadCheck>
              <MediaUpload 
                // onSelect={onFileSelect} 
                onSelect={event => onFileSelect(event, 'mobile')} 
                value={props.attributes.mobileImgID} 
                render={({ open }) => {
                  return <Button onClick={open}>Choose Mobile Image</Button>
                }} 
              />
            </MediaUploadCheck>
            <MediaUploadCheck>
              <MediaUpload 
                onSelect={event => onFileSelect(event)} 
                value={props.attributes.imgID} 
                render={({ open }) => {
                  return <Button onClick={open}>Choose Desktop Image</Button>
                }} 
              />
            </MediaUploadCheck>
          </PanelRow>
        </PanelBody>
        <PanelBody title="Color Fondo Cintillo" initialOpen={false}>
            <PanelRow>
              <ColorPalette
                  value={ props.attributes.cintilloBackgroundColor }
                  colors={ [ ...useSetting( 'color.palette' ) ] }
                  onChange={ ( value ) => props.setAttributes( { cintilloBackgroundColor: value } ) }
              />
            </PanelRow>
          </PanelBody>
      </InspectorControls>
        <div className="pp__hero_text">
            <InnerBlocks
              template = { PP_TEMPLATE }
              allowedBlocks={["core/paragraph","core/shortcode"]}
            />
        </div>
        <picture className="pp__hero_image">
          <source media="(max-width: 1023px)" srcset={props.attributes.mobileImgURL} alt="Pura Prote"/>
          <source media="(min-width: 1024px)" srcset={props.attributes.imgURL} alt="Pura Prote"/>
          <img src={props.attributes.imgURL} alt="Pura Prote"/>
        </picture>
    </div> 
    <RichText 
      tagName="p" 
      className={`pp__cintillo hasmedium-font-size`} 
      value={props.attributes.cintillo} 
      onChange={handleCintilloChange} 
      placeholder={ 'Cintillo...' } 
    />
    </>
  )
}

function SaveComponent() {
  return <InnerBlocks.Content /> 
  // InnerBlocks.content in case it has nested blocks, otherwise it could be null
}