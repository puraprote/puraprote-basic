<?php
// echo '<pre>';
// var_dump($attributes);
// echo '</pre>';

$class="pp__grid";
if (isset($attributes['className'])) {
  $class .= " ".$attributes['className'];
}

if (isset($attributes['align'])) {
    $class .= ' align'.$attributes['align'];
    }

$style = '';

// $min_width = '100px';
if (isset($attributes['minWidth'])) {
//    $min_width = $attributes['minWidth'];

   $style = '--pp-grid--min-width: '.$attributes['minWidth'].';';
}

// $gap = '0';
if (isset($attributes['gap'])) {
    $gap = $attributes['gap'];

    $style .= ' grid-gap: '.$attributes['gap'].';';
}

if (isset($attributes['style']['spacing'])) {
   $style .= ' '.pp_get_spacing_values($attributes['style']['spacing']);
}


// $style = 'grid-template-columns: repeat(auto-fit, minmax('.$attributes['minWidth'].', 1fr)); grid-gap: '.$gap.';';

if ( $style != '' ) {
    $style= ' style="'.$style.'"';
}



?>
<div class="pp__grid"<?php echo $style; ?>>
<?php echo $content; ?>
</div>