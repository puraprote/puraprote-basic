<?php
// echo '<pre>';
// var_dump($attributes);
// var_dump(get_block_wrapper_attributes());
// echo '</pre>';

$url = get_site_url();
if (isset($attributes['linkObject'])) {
    $url = $attributes['linkObject']['url'];
}


$class = "pp__box";
if (isset($attributes['className'])) {
  $class .= " ".$attributes['className'];
}


$style = '';
if (isset($attributes['style']['spacing'])) {
   $style = ' style="'.pp_get_spacing_values($attributes['style']['spacing']).'"';
}

?>

<a href="<?php echo $url; ?>" class="<?php echo $class; ?> pp__button"<?php echo $style; ?>>
<?php echo $content; ?>
</a>  
