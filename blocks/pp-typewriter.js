import { ToolbarGroup, ToolbarButton } from "@wordpress/components"
import { RichText, InspectorControls, BlockControls } from "@wordpress/block-editor"

import { registerBlockType } from "@wordpress/blocks"

registerBlockType("pp-theme/pp-typewriter",{
    title: "PP typewriter",
    icon: 'heading',
    supports: {
        color: {
            text: true,
            background: true,
            link: true
        },
        spacing: {
            margin: [ 'top', 'bottom' ],  // Enable margin UI control.
            padding: true, // Enable padding UI control.
            blockGap: false,  // Enables block spacing UI control for blocks that also use `layout`.
          },
        dimenstions: true
    },
    attributes: {
        text: {type: "string"},
        size: {type: "string", default: "large"},
        colorName: {type: "string", default: "blue"}
    },
    edit: EditComponent,
    save: SaveComponent
}) 

function EditComponent(props) {

    function handleTextChange(x) {
        props.setAttributes({text: x})
    }


    return (
        <>
            <InspectorControls>
            </InspectorControls>
            <RichText 
                allowedFormats={["core/bold","core/italic"]} 
                tagName="p" 
                className={`pp__heading has-medium-font-size`} 
                value={props.attributes.text} onChange={handleTextChange} 
                placeholder={ 'Typewriter text here...' }
            />
        </>       
    )
}

function SaveComponent(props) {
    return null
}