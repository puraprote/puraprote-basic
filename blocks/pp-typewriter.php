<?php
// echo '<pre>';
// var_dump($attributes);
// echo '</pre>';

// if ( !isset($attributes['backgroundColor'])) {
//     $attributes['backgroundColor'] = 'gray';
// }

// if ( !isset($attributes['textColor'])) {
//     $attributes['textColor'] = 'base';
// }


$class='has-'.$attributes['backgroundColor'].'-background-color has-background has-'.$attributes['textColor'].'-color has-text-color';


$style_container = '';
if (isset($attributes['style']['spacing'])) {
   $style_container = ' style="'.pp_get_spacing_values($attributes['style']['spacing'],'margin').'"';
}



?>

<p class="pp__typewriter <?php echo $class; ?>"<?php echo $style_container; ?>>
    <?php echo $attributes['text']; ?>
    <div id="typedtext" style="margin: 1rem;"></div>
</p>
