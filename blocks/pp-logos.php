<?php

// echo '<pre>';
// var_dump($attributes);
// echo '</pre>';

$class = 'pp__logos';

if (isset($attributes['align'])) {
    $class .= ' align'.$attributes['align'];
}

if (isset($attributes['logoCount'])) {
    $logo_count = $attributes['logoCount'];
} else {
    $logo_count = 10;
}

if (isset($attributes['logoWidth'])) {
    $logo_width = $attributes['logoWidth'];
} else {
    $logo_width = '18rem';
}

?>

<style>
    .pp__logos {
        --pp__logos--logo_width: <?php echo $logo_width; ?>;
        --pp__logos--count: <?php echo $logo_count; ?>;
    }
</style>

<div class="<?php echo $class; ?>">
    <?php echo $content; ?>
    <?php echo $content; ?>
    <?php echo $content; ?>
</div>