<?php
// if ( !isset($attributes['imgURL'])) {
//   $attributes['imgURL'] = get_theme_file_uri('/assets/images/protes-home-desk.jpg');
// }

// echo '<pre>';
// var_dump($attributes);
// echo '</pre>';


if ( !isset($attributes['imgURL'])) {
    $attributes['imgURL'] = get_theme_file_uri('/assets/images/protes-home-desk.jpg');
  }
$class="pp__hero pp__hero--home alignfull";

if (isset($attributes['textColor'])) {
  $class .= ' has-'.$attributes['textColor'].'-color has-text-color';
}
?>
  <header class="<?php echo $class; ?>">
    <h1 class="pp__heading has-xx-large-font-size">
        <?php echo $attributes['heading'] ?>
    </h1>

    <div class="pp__hero_text">
      <?php echo $content; ?>
    </div>
  
    <picture class="pp__hero_image">
      <source media="(max-width: 1023px)" srcset="<?php echo $attributes['mobileImgURL'] ?>" alt="Pura Prote"/>
      <source media="(min-width: 1024px)" srcset="<?php echo $attributes['imgURL'] ?>" alt="Pura Prote"/>
      <img src="<?php echo $attributes['imgURL'] ?>" alt="Pura Prote"/>
    </picture>
  
  </header>

<?php
require_once(get_template_directory().'/partials/cintillo.php');
