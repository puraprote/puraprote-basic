import { ToolbarGroup, ToolbarButton } from "@wordpress/components"
import { RichText, InspectorControls, BlockControls, useBlockProps,AlignmentControl } from "@wordpress/block-editor"

import { registerBlockType } from "@wordpress/blocks"

registerBlockType("pp-theme/pp-paragraph-plain",{
    title: "PP Paragraph PLAIN",
    parent: [ 'pp-theme/pp-button' ],
    category: 'theme',
    icon: 'editor-paragraph',
    supports: {
        color: {
            text: true,
            background: false,
            link: true
        }
    },
    attributes: {
        text: {type: "string"},
        colorName: {type: "string", default: "blue"},
        alignText: {type: "string", default: "none"}
    },
    edit: EditComponent,
    save: SaveComponent
}) 

function EditComponent(props) {

    const blockProps = useBlockProps();

    function handleTextChange(x) {
        props.setAttributes({text: x})
    }

    const onChangeAlign = ( newAlign ) => {
        props.setAttributes( { 
            alignText: newAlign === undefined ? 'none' : newAlign, 
        } )
    }


    return (
        <>
			<BlockControls>
				<AlignmentControl
					value={ props.attributes.alignText }
					onChange={ onChangeAlign }
				/>
			</BlockControls>
            <InspectorControls>
            </InspectorControls>
            <RichText 
                tagName={"p"} 
                allowedFormats={["core/bold","core/italic","core/image","core/strikethrough","font-awesome/icon","jvm/insert-icons"]}
                className={`has-default-font-size`} 
                value={props.attributes.text} 
                onChange={handleTextChange} 
                style={ { textAlign: props.attributes.alignText } }
                placeholder={ 'Paragraph...' }
            />
        </>
    )
}

function SaveComponent(props) {
    return null
}