import { ToolbarGroup, ToolbarButton } from "@wordpress/components"
import { RichText, InspectorControls, BlockControls, useBlockProps,AlignmentControl } from "@wordpress/block-editor"
import { PluginBlockSettingsMenuItem } from '@wordpress/edit-post';

import { registerBlockType } from "@wordpress/blocks"

registerBlockType("pp-theme/pp-heading-plain",{
    title: "PP Heading PLAIN",
    parent: [ 'pp-theme/pp-button' ],
    category: 'theme',
    icon: 'heading',
    supports: {
        color: {
            text: true,
            background: false,
            link: true
        }
    },
    attributes: {
        text: {type: "string"},
        headingLevel: {type: "string", default: "large"},
        colorName: {type: "string", default: "blue"},
        alignText: {type: "string", default: "none"}
    },
    edit: EditComponent,
    save: SaveComponent
}) 

function EditComponent(props) {

    const blockProps = useBlockProps();

    function handleTextChange(x) {
        props.setAttributes({text: x})
    }

    const onChangeAlign = ( newAlign ) => {
        props.setAttributes( { 
            alignText: newAlign === undefined ? 'none' : newAlign, 
        } )
    }


    return (
        <>
			<BlockControls>
				<AlignmentControl
					value={ props.attributes.alignText }
					onChange={ onChangeAlign }
				/>
                <ToolbarGroup>
                    <ToolbarButton isPressed={props.attributes.headingLevel === "h2"} onClick={() => props.setAttributes({ headingLevel: "h2" })}>
                        H2
                    </ToolbarButton>
                    <ToolbarButton isPressed={props.attributes.headingLevel === "h3"} onClick={() => props.setAttributes({ headingLevel: "h3" })}>
                        H3
                    </ToolbarButton>
                    <ToolbarButton isPressed={props.attributes.headingLevel === "h4"} onClick={() => props.setAttributes({ headingLevel: "h4" })}>
                        H4
                    </ToolbarButton>
                </ToolbarGroup>
			</BlockControls>
            <InspectorControls>
            </InspectorControls>
            <RichText 
                { ...blockProps }
                tagName={"h2"} 
                allowedFormats={["core/bold","core/italic","core/image","core/strikethrough","font-awesome/icon","jvm/insert-icons"]}
                className={`pp__heading has-large-font-size`} 
                value={props.attributes.text} 
                onChange={handleTextChange} 
                style={ { textAlign: props.attributes.alignText } }
                placeholder={ 'Heading...' }
            />
        </>
    )
}

function SaveComponent(props) {
    return null
}