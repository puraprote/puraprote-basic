<?php

// echo '<pre>';
// var_dump($attributes);
// echo '</pre>';

if ( !isset($attributes['imgURL'])) {
  $attributes['imgURL'] = get_theme_file_uri('/assets/images/protes-home-desk.jpg');
}


?>
<header class="pp__hero alignfull">
  <div class="pp__hero_text">
    <?php echo $content; ?>
  </div>

  <picture class="pp__hero_image">
    <source media="(max-width: 1023px)" srcset="<?php echo $attributes['mobileImgURL'] ?>" alt="Pura Prote"/>
      <source media="(min-width: 1024px)" srcset="<?php echo $attributes['imgURL'] ?>" alt="Pura Prote"/>
    <img src="<?php echo $attributes['imgURL'] ?>" alt="Pura Prote"/>
  </picture>


</header>

<?php
require_once(get_template_directory().'/partials/cintillo.php');

