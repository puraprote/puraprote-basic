import { InnerBlocks } from "@wordpress/block-editor"
import { registerBlockType } from "@wordpress/blocks"

registerBlockType("pp-theme/pp-topbars",{
    title: "PP Topbars",
    edit: EditComponent,
    save: SaveComponent
}) 

function EditComponent() {
    return (
        <!-- wp:group {"style":{"spacing":{"padding":{"top":"var:preset|spacing|40","bottom":"var:preset|spacing|40","right":"var:preset|spacing|40","left":"var:preset|spacing|40"}}},"backgroundColor":"gray","textColor":"contrast","layout":{"type":"constrained"}} -->
        <div class="wp-block-group has-contrast-color has-gray-background-color has-text-color has-background" style="padding-top:var(--wp--preset--spacing--40);padding-right:var(--wp--preset--spacing--40);padding-bottom:var(--wp--preset--spacing--40);padding-left:var(--wp--preset--spacing--40)"><!-- wp:group {"align":"full","layout":{"type":"flex","justifyContent":"space-between","flexWrap":"wrap"}} -->
        <div class="wp-block-group alignfull"><!-- wp:group {"layout":{"type":"flex"}} -->
        <div class="wp-block-group"><!-- wp:image {"id":21,"width":40,"sizeSlug":"full","linkDestination":"none"} -->
        <figure class="wp-block-image size-full is-resized"><img src="./wp-content/themes/puraprote-basic/assets/images/costa-rica.png" alt="" class="wp-image-21" style="width:40px" width="40"/></figure>
        <!-- /wp:image -->

        <!-- wp:navigation {"ref":20} /--></div>
        <!-- /wp:group -->

        <!-- wp:navigation {"ref":19,"overlayBackgroundColor":"base","overlayTextColor":"contrast"} /--></div>
        <!-- /wp:group --></div>
        <!-- /wp:group -->

        <!-- wp:group {"style":{"spacing":{"padding":{"top":"var:preset|spacing|50","bottom":"var:preset|spacing|50","right":"var:preset|spacing|50","left":"var:preset|spacing|50"},"margin":{"top":"0","bottom":"0"},"blockGap":"0"}},"backgroundColor":"base","layout":{"type":"constrained"}} -->
        <div class="wp-block-group has-base-background-color has-background" style="margin-top:0;margin-bottom:0;padding-top:var(--wp--preset--spacing--50);padding-right:var(--wp--preset--spacing--50);padding-bottom:var(--wp--preset--spacing--50);padding-left:var(--wp--preset--spacing--50)"><!-- wp:group {"align":"full","layout":{"type":"flex","justifyContent":"space-between","flexWrap":"wrap"}} -->
        <div class="wp-block-group alignfull"><!-- wp:site-logo {"width":136,"shouldSyncIcon":true} /-->

        <!-- wp:navigation {"ref":5,"overlayMenu":"never","overlayBackgroundColor":"base","overlayTextColor":"contrast","layout":{"type":"flex","justifyContent":"center"}} /-->

        <!-- wp:navigation {"ref":23,"overlayBackgroundColor":"base","overlayTextColor":"contrast","layout":{"type":"flex","justifyContent":"right"}} /--></div>
        <!-- /wp:group --></div>
        <!-- /wp:group -->
        )
    }
    
    function SaveComponent() {
        return (
            <!-- wp:group {"style":{"spacing":{"padding":{"top":"var:preset|spacing|40","bottom":"var:preset|spacing|40","right":"var:preset|spacing|40","left":"var:preset|spacing|40"}}},"backgroundColor":"gray","textColor":"contrast","layout":{"type":"constrained"}} -->
            <div class="wp-block-group has-contrast-color has-gray-background-color has-text-color has-background" style="padding-top:var(--wp--preset--spacing--40);padding-right:var(--wp--preset--spacing--40);padding-bottom:var(--wp--preset--spacing--40);padding-left:var(--wp--preset--spacing--40)"><!-- wp:group {"align":"full","layout":{"type":"flex","justifyContent":"space-between","flexWrap":"wrap"}} -->
            <div class="wp-block-group alignfull"><!-- wp:group {"layout":{"type":"flex"}} -->
            <div class="wp-block-group"><!-- wp:image {"id":21,"width":40,"sizeSlug":"full","linkDestination":"none"} -->
            <figure class="wp-block-image size-full is-resized"><img src="./wp-content/themes/puraprote-basic/assets/images/costa-rica.png" alt="" class="wp-image-21" style="width:40px" width="40"/></figure>
            <!-- /wp:image -->
    
            <!-- wp:navigation {"ref":20} /--></div>
            <!-- /wp:group -->
    
            <!-- wp:navigation {"ref":19,"overlayBackgroundColor":"base","overlayTextColor":"contrast"} /--></div>
            <!-- /wp:group --></div>
            <!-- /wp:group -->
    
            <!-- wp:group {"style":{"spacing":{"padding":{"top":"var:preset|spacing|50","bottom":"var:preset|spacing|50","right":"var:preset|spacing|50","left":"var:preset|spacing|50"},"margin":{"top":"0","bottom":"0"},"blockGap":"0"}},"backgroundColor":"base","layout":{"type":"constrained"}} -->
            <div class="wp-block-group has-base-background-color has-background" style="margin-top:0;margin-bottom:0;padding-top:var(--wp--preset--spacing--50);padding-right:var(--wp--preset--spacing--50);padding-bottom:var(--wp--preset--spacing--50);padding-left:var(--wp--preset--spacing--50)"><!-- wp:group {"align":"full","layout":{"type":"flex","justifyContent":"space-between","flexWrap":"wrap"}} -->
            <div class="wp-block-group alignfull"><!-- wp:site-logo {"width":136,"shouldSyncIcon":true} /-->
    
            <!-- wp:navigation {"ref":5,"overlayMenu":"never","overlayBackgroundColor":"base","overlayTextColor":"contrast","layout":{"type":"flex","justifyContent":"center"}} /-->
    
            <!-- wp:navigation {"ref":23,"overlayBackgroundColor":"base","overlayTextColor":"contrast","layout":{"type":"flex","justifyContent":"right"}} /--></div>
            <!-- /wp:group --></div>
            <!-- /wp:group -->
        )
    }