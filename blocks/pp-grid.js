import { Button, PanelBody, PanelRow, __experimentalNumberControl as NumberControl, __experimentalUnitControl as UnitControl} from "@wordpress/components"
import { RichText, InnerBlocks, InspectorControls, useBlockProps, useSetting } from "@wordpress/block-editor"
import { registerBlockType } from "@wordpress/blocks"
import { useEffect } from "@wordpress/element" //React


//SRC: https://wordpress.stackexchange.com/questions/393524/how-to-add-template-colors-to-custom-block-options-in-wordpress-gutenberg-editor

registerBlockType("pp-theme/pp-grid",{
    title: "PP Grid Flexible",
    supports: {
        color: {
            text: true,
            background: true,
            link: true
        },
      align: true,
      spacing: {
        margin: true,  // Enable margin UI control.
        padding: true, // Enable padding UI control.
        blockGap: true,  // Enables block spacing UI control for blocks that also use `layout`.
      }
    },
    icon: 'dashicons-grid-view',
    attributes: {
    //   align: { type: "string", default: "full" },
      minWidth: { type: "string", default: "210px" },
      // maxCol: { type: "number", default: "4" },
      gap: { type: "string", default: "0" }
    },
    edit: EditComponent,
    save: SaveComponent
}) 

function EditComponent(props) {

  const blockProps = useBlockProps( {
    className: 'pp__grid',
  } );



  const PP_TEMPLATE = [
    [
      'core/group', 
      {}, 
      [
        [
          'core/paragraph', 
          {
            placeholder: 'Texto'
          }
        ]
      ]
    ]
  ];

  return (
    <>
    <div { ...blockProps }>
      <InspectorControls>

         <PanelBody title="Configuración Columnas" initialOpen={true}>
            <PanelRow>
                <UnitControl 
                    label={ 'Ancho mínimo de columnas:' } 
                    onChange={ ( value ) => props.setAttributes( { minWidth: value } ) } 
                    value={ props.attributes.minWidth } 
                />
            </PanelRow>
            <PanelRow>
                <UnitControl 
                    label={ 'Espacio entre columnas:' } 
                    onChange={ ( value ) => props.setAttributes( { gap: value } ) } 
                    value={ props.attributes.gap } 
                />
            </PanelRow>
            {/* <PanelRow>
                <NumberControl
                    isShiftStepEnabled={ true }
                    label={ 'Cantidad máxima columnas:' } 
                    // labelPosition={ 'side' }
                    shiftStep={ 1 }
                    value={ props.attributes.maxCol }
                    onChange={ ( value ) => props.setAttributes( { maxCol: parseInt(value) } ) }
                />
            </PanelRow> */}

          </PanelBody> 
      </InspectorControls>
        <div className="pp__grid">
            <InnerBlocks
              template = { PP_TEMPLATE }
            //   allowedBlocks={["core/paragraph","core/shortcode"]}
            />
        </div>
    </div> 

    </>
  )
}

function SaveComponent() {
  return <InnerBlocks.Content /> 
  // InnerBlocks.content in case it has nested blocks, otherwise it could be null
}