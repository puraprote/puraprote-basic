<?php
// echo '<pre>';
// var_dump($attributes);
// echo '</pre>';
$class = 'pp__heading has-large-font-size';
$heading_level = 'h2';
if (isset($attributes['headingLevel'])) {
    $heading_level = $attributes['headingLevel'];
}
if (isset($attributes['textColor'])) {
    $class .= ' has-'.$attributes['textColor'].'-color has-text-color';
}
if (isset($attributes['alignText'])) {
    $class .= ' has-text-align-'.$attributes['alignText'];
}
?>

<<?php echo $heading_level; ?> class="<?php echo $class; ?>"><?php echo $attributes['text']; ?></<?php echo $heading_level; ?>>