<?php
// echo '<pre>';
// var_dump($attributes);
// echo '</pre>';
$class = 'has-default-font-size';

if (isset($attributes['textColor'])) {
    $class .= ' has-'.$attributes['textColor'].'-color has-text-color';
}
if (isset($attributes['alignText'])) {
    $class .= ' has-text-align-'.$attributes['alignText'];
}
?>

<p class="<?php echo $class; ?>"><?php echo $attributes['text']; ?></p>