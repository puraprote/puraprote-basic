import { InnerBlocks } from "@wordpress/block-editor"
import { registerBlockType } from "@wordpress/blocks"

registerBlockType("pp-theme/pp-footer",{
    title: "PP Footer",
    edit: EditComponent,
    save: SaveComponent
}) 

function EditComponent() {
    return (
        <div className="banner_healthy">
            <ul>
                <li><img src="/wp-content/themes/puraprote-basic/assets/images/icon-preservantes.png" /></li>
                <li><img src="/wp-content/themes/puraprote-basic/assets/images/icon-edulcorantes.png" /></li>
                <li><img src="/wp-content/themes/puraprote-basic/assets/images/icon-saborizantes.png" /></li>
            </ul>
            <p>Libre de saborizantes, edulcorantes, preservantes y otros aditivos. Opciones keto y veganas.</p>
            <ul>
                <li><img src="/wp-content/themes/puraprote-basic/assets/images/icon-emulsificantes.png" /></li>
                <li><img src="/wp-content/themes/puraprote-basic/assets/images/icon-keto.png" /></li>
                <li><img src="/wp-content/themes/puraprote-basic/assets/images/icon-vegana.png" /></li>
            </ul>
        </div>
    )
}

function SaveComponent() {
    return (
        <div className="banner_healthy">
            <ul>
                <li><img src="/wp-content/themes/puraprote-basic/assets/images/icon-preservantes.png" /></li>
                <li><img src="/wp-content/themes/puraprote-basic/assets/images/icon-edulcorantes.png" /></li>
                <li><img src="/wp-content/themes/puraprote-basic/assets/images/icon-saborizantes.png" /></li>
            </ul>
            <p>Libre de saborizantes, edulcorantes, preservantes y otros aditivos. Opciones keto y veganas.</p>
            <ul>
                <li><img src="/wp-content/themes/puraprote-basic/assets/images/icon-emulsificantes.png" /></li>
                <li><img src="/wp-content/themes/puraprote-basic/assets/images/icon-keto.png" /></li>
                <li><img src="/wp-content/themes/puraprote-basic/assets/images/icon-vegana.png" /></li>
            </ul>
        </div>
    )
}