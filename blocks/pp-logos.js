import { Button, PanelBody, PanelRow, ColorPalette, __experimentalNumberControl as NumberControl, __experimentalUnitControl as UnitControl } from "@wordpress/components"
import { RichText, InnerBlocks, InspectorControls, MediaUpload, MediaUploadCheck, useBlockProps, useSetting } from "@wordpress/block-editor"
import { registerBlockType } from "@wordpress/blocks"
import { useEffect } from "@wordpress/element" //React


//SRC: https://wordpress.stackexchange.com/questions/393524/how-to-add-template-colors-to-custom-block-options-in-wordpress-gutenberg-editor

registerBlockType("pp-theme/pp-logos",{
    title: "PP Logos",
    supports: {
      align: true,
      spacing: {
        margin: true,  // Enable margin UI control.
        padding: true, // Enable padding UI control.
        blockGap: true,  // Enables block spacing UI control for blocks that also use `layout`.
      }
    },
    icon: 'cover-image',
    attributes: {
      // align: { type: "string", default: "full" },
      logoCount: { type: "number", default: "3" },
      logoWidth: { type: "string", default: "18rem" },
    },
    edit: EditComponent,
    save: SaveComponent
}) 

function EditComponent(props) {

  const blockProps = useBlockProps( {
    className: 'pp__logos',
  } );

  const PP_TEMPLATE = [
    [ 'core/image' ],
    [ 'core/image' ],
    [ 'core/image' ],
  ];

  return (
    <>
    <div { ...blockProps }>
      <InspectorControls>
        <PanelBody title="Configuracion" initialOpen={true}>
            <PanelRow>
                <NumberControl
                    isShiftStepEnabled={ true }
                    label={ 'Cantidad de logos:' } 
                    // labelPosition={ 'side' }
                    shiftStep={ 1 }
                    value={ props.attributes.logoCount }
                    onChange={ ( value ) => props.setAttributes( { logoCount: parseInt(value) } ) }
                />
            </PanelRow>
            <PanelRow>
                <UnitControl 
                    label={ 'Ancho de recuadro contenedor de cada logo:' } 
                    onChange={ ( value ) => props.setAttributes( { logoWidth: value } ) } 
                    value={ props.attributes.logoWidth } 
                />
            </PanelRow>
        </PanelBody>
      </InspectorControls>
        <div>
            <InnerBlocks
              template = { PP_TEMPLATE }
              allowedBlocks={["core/image"]}
            />
        </div>

    </div> 
    </>
  )
}

function SaveComponent() {
  return <InnerBlocks.Content /> 
  // InnerBlocks.content in case it has nested blocks, otherwise it could be null
}