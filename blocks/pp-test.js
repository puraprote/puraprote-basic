import { ToolbarGroup, ToolbarButton } from "@wordpress/components"
import { RichText, InspectorControls, BlockControls, useBlockProps,AlignmentControl,RichTextToolbarButton } from "@wordpress/block-editor"
import { toggleFormat, registerFormatType } from '@wordpress/rich-text';
import { registerBlockType } from "@wordpress/blocks"

registerBlockType("pp-theme/pp-test",{
    apiVersion: 2,
    title: "PP Test",
    supports: {
        color: {
            text: true,
            background: false,
            link: true
        }
    },
    icon: 'heading', // https://developer.wordpress.org/resource/dashicons/#heading
    category: 'text',
    attributes: {
        subheading: {
        type: 'string',
        },
        heading: {
        type: 'string',
        },
        content: {
        type: 'string',
        }
    },
    edit: EditComponent,
    save: SaveComponent
}) 

function EditComponent(props) {

    const blockProps = useBlockProps();

    function handleTextChange(x) {
        props.setAttributes({text: x})
    }

    const onChangeAlign = ( newAlign ) => {
        props.setAttributes( { 
            align: newAlign === undefined ? 'none' : newAlign, 
        } )
    }

    const {
        attributes,
        setAttributes
      } = props;
  
      const {
        subheading,
        heading,
        content
      } = attributes;


    return (
        
        <div {...blockProps}>
        <RichText
          tagName='h3'
          className='card-subheading'
          value={subheading}
          onChange={(newVal) => setAttributes({subheading: newVal})}
          placeholder="Subheading Goes Here"
        />
        <RichText
          tagName='h1'
          className='card-heading'
          value={heading}
          onChange={(newVal) => setAttributes({heading: newVal})}
          placeholder="Heading Goes Here"
        />
        <RichText
          tagName='div'
          className='card-content'
          multiline='p'
          value={content}
          onChange={(newVal) => setAttributes({content: newVal})}
          placeholder="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        />
      </div>
    )
}

function SaveComponent(props) {
    return null
}