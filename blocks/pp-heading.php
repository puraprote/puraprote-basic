<?php
$class = 'pp__heading has-xx-large-font-size';
if (isset($attributes['textColor'])) {
    $class .= ' has-'.$attributes['textColor'].'-color has-text-color';
}
?>

<h1 class="<?php echo $class; ?>"><?php echo $attributes['text']; ?></h1>