import { ToolbarGroup, ToolbarButton, TextControl, Flex, FlexBlock, FlexItem, Button, Icon, PanelBody, PanelRow, ColorPicker } from "@wordpress/components"
import { RichText, InspectorControls, BlockControls, AlignmentToolbar } from "@wordpress/block-editor"

import { registerBlockType } from "@wordpress/blocks"

registerBlockType("pp-theme/pp-text-animation",{
    title: "PP Text Animation",
    icon: 'heading',
    supports: {
        color: {
            text: true,
            background: true,
            link: true
        },
        spacing: {
            margin: [ 'top', 'bottom' ],  // Enable margin UI control.
            padding: true, // Enable padding UI control.
            blockGap: false,  // Enables block spacing UI control for blocks that also use `layout`.
          },
        dimenstions: true
    },
    attributes: {
        text: { type: "array", default: [""] },
        size: {type: "string", default: "large"},
        colorName: {type: "string", default: "blue"}
    },
    edit: EditComponent,
    save: SaveComponent
}) 

function EditComponent(props) {

    function deleteText(indexToDelete) {
        const newAnswers = props.attributes.text.filter(function (x, index) {
          return index != indexToDelete
        })
        props.setAttributes({ text: newAnswers })
    
        // if (indexToDelete == props.attributes.correctAnswer) {
        //   props.setAttributes({ correctAnswer: undefined })
        // }
    }


    return (
        <div className="paying-attention-edit-block" style={{ backgroundColor: props.attributes.bgColor }}>
            {/* <BlockControls>
                <AlignmentToolbar value={props.attributes.theAlignment} onChange={x => props.setAttributes({ theAlignment: x })} />
            </BlockControls> */}
            {/* <InspectorControls>
                <PanelBody title="Background Color" initialOpen={true}>
                <PanelRow>
                    <ChromePicker color={props.attributes.bgColor} onChangeComplete={x => props.setAttributes({ bgColor: x.hex })} disableAlpha={true} />
                </PanelRow>
                </PanelBody>
            </InspectorControls> */}

            <p style={{ fontSize: "13px", margin: "20px 0 8px 0" }}>Textos:</p>
            {props.attributes.text.map(function (text, index) {
                return (
                <Flex>
                    <FlexBlock>
                        <RichText
                            tagName='p'
                            className='thingy'
                            // multiline='p'
                            value={text}
                            onChange={newValue => {
                                const newTexts = props.attributes.text.concat([])
                                newTexts[index] = newValue
                                props.setAttributes({ text: newTexts })
                                }}
                            placeholder="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
                        />
                    </FlexBlock>
                    <FlexItem>
                        <Button isLink className="attention-delete" onClick={() => deleteText(index)}>
                            Delete
                        </Button>
                    </FlexItem>
                </Flex>
                )
            })}
            <Button
                isPrimary
                onClick={() => {
                props.setAttributes({ text: props.attributes.text.concat([undefined]) })
                }}
            >
                Añadir otro texto
            </Button>
            </div>
    )
}

function SaveComponent(props) {
    return null
}