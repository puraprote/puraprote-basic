<?php
// echo '<pre>';
// var_dump($attributes);
// echo '</pre>';

if ( !isset($attributes['backgroundColor'])) {
    $attributes['backgroundColor'] = 'gray';
}

if ( !isset($attributes['textColor'])) {
    $attributes['textColor'] = 'base';
}


$class='has-'.$attributes['backgroundColor'].'-background-color has-background has-'.$attributes['textColor'].'-color has-text-color';


$style_container = '';
if (isset($attributes['style']['spacing'])) {
   $style_container = ' style="'.pp_get_spacing_values($attributes['style']['spacing'],'margin').'"';
}

$style_slide = '';
if (isset($attributes['style']['spacing'])) {
   $style_slide = ' style="'.pp_get_spacing_values($attributes['style']['spacing'],'padding').'"';
}



?>

<div class="glide <?php echo $class; ?>"<?php echo $style_container; ?>>
  <div class="glide__track" data-glide-el="track">
    <ul class="glide__slides">

        <?php 
        if (is_array($attributes['text']) && !empty($attributes['text'])) {
            foreach ($attributes['text'] as $text) { ?>
                <li class="glide__slide"<?php echo $style_slide; ?>>
            
                    <?php echo $text; ?>

                </li>
            <?php }
        }
        ?>

    </ul>
  </div>

</div>
