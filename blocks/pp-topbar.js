import { PanelBody, PanelRow } from "@wordpress/components"
import { InnerBlocks, InspectorControls } from "@wordpress/block-editor"
import { registerBlockType } from "@wordpress/blocks"

registerBlockType("pp-theme/pp-topbar",{
    title: "PP Topbar",
    supports: {
        align: ["full", "wide"],
        color: {
            text: true,
            background: false,
            link: true
        },
        spacing: {
          margin: true,  // Enable margin UI control.
          padding: true, // Enable padding UI control.
          blockGap: true,  // Enables block spacing UI control for blocks that also use `layout`.
        }
    },
    attributes: {
        align: {type: "string", default: "full"}
    },
    edit: EditComponent,
    save: SaveComponent
}) 

function EditComponent() {

    return (
        <>
            <div className="pp__topbar--row pp__topbar--row__3col">
                <InnerBlocks allowedBlocks={["core/group", "core/site-logo","core/image","core/navigation"]} /> 
            </div>
        </>
    )
}

function SaveComponent() {
    return (
        <>
            <div className="pp__topbar--row pp__topbar--row__3col">
                <InnerBlocks.Content />
            </div>
        </>
        
    )
}