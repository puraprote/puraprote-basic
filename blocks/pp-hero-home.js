import { Button, PanelBody, PanelRow, Spinner, BaseControl } from "@wordpress/components"
import { RichText, InnerBlocks, useBlockProps, InspectorControls, MediaUpload, MediaUploadCheck } from "@wordpress/block-editor"
import { registerBlockType } from "@wordpress/blocks"


registerBlockType("pp-theme/pp-hero-home",{
    title: "PP Hero Home",
    icon: 'cover-image',
    supports: {
      align: ["full"],
      color: {
        text: true,
        background: false,
        link: true
      },
      spacing: {
        margin: true,  // Enable margin UI control.
        padding: true, // Enable padding UI control.
        blockGap: true,  // Enables block spacing UI control for blocks that also use `layout`.
      }
    },
    attributes: {
        align: { type: "string", default: "full" },
        mobileImgID: { type: "number" },
        mobileImgURL: { type: "string" },
        imgID: { type: "number" },
        imgURL: { type: "string", default: "https://fakeimg.pl/1200x400/EFEEED/eee/?text=Imagen" },
        heading: { type: "string" },
        cintillo: { type: "string" },
        cintilloImages: {
          type: 'array',
          // source: 'query',
          // selector: '.slider-item',
          default: [],
        },
        headingColor: { type: "string" }
      },
    edit: EditComponent,
    save: SaveComponent
}) 

function EditComponent(props) {

  const blockProps = useBlockProps( {
    className: 'pp__hero pp__hero--home',
  } );

  const PP_TEMPLATE = [
    [ 'core/paragraph', { placeholder: 'Texto' } ],
    [ 'mailchimp-for-wp/form' ],
  ];

    function onFileSelect(x, whichImg = 'desktop') {
      // Log to console so we can see what WP gives us
      console.log(whichImg)
      console.log(x)
      console.log(x.sizes.full.url)
    
      // WP doesn't give us custom image sizes, so we'll have to grab it based on ID
  
      if ( whichImg == 'mobile' ) {
        props.setAttributes({ mobileImgID: x.id })
        props.setAttributes({ mobileImgURL: x.sizes.full.url })
      } else {
        props.setAttributes({ imgID: x.id })
        props.setAttributes({ imgURL: x.sizes.full.url })
      }
  
      
    }

    function handleCintilloChange(x) {
      props.setAttributes({cintillo: x})
    }

    function handleHeadingChange(x) {
        props.setAttributes({heading: x})
    }


    
  
    return (
      <>
      <div {...blockProps }>
        <InspectorControls>
          <PanelBody title="Background" initialOpen={true}>
            <PanelRow>
              <MediaUploadCheck>
                <MediaUpload 
                  // onSelect={onFileSelect} 
                  onSelect={event => onFileSelect(event, 'mobile')} 
                  value={props.attributes.mobileImgID} 
                  render={({ open }) => {
                    return <Button onClick={open}>Choose Mobile Image</Button>
                  }} 
                />
              </MediaUploadCheck>
              <MediaUploadCheck>
                <MediaUpload 
                  onSelect={event => onFileSelect(event)} 
                  value={props.attributes.imgID} 
                  render={({ open }) => {
                    return <Button onClick={open}>Choose Desktop Image</Button>
                  }} 
                />
              </MediaUploadCheck>
            </PanelRow>
          </PanelBody>
          <PanelBody title="Imagenes Cintillo" initialOpen={false}>
            <PanelRow>
              <MediaUploadCheck>
                <MediaUpload 
                  onSelect={ ( media ) =>
                    props.setAttributes( { cintilloImages: media.map(image => image.id) } )
                  }
                  multiple={true}
                  allowedTypes={ ['image'] }
                  value={ props.attributes.cintilloImages }
                  render={ ( { open } ) => (
                    <div>
                      <Button variant="secondary" onClick={ open }>Add images</Button>
                    </div>
                  ) }
                />
              </MediaUploadCheck>
            </PanelRow>
          </PanelBody>
        </InspectorControls>
        <InspectorControls>
            </InspectorControls>
  
            <RichText 
              tagName="h1" 
              className={`pp__heading has-xx-large-font-size`} 
              value={props.attributes.heading} 
              onChange={handleHeadingChange} 
              placeholder={ 'Heading...' } 
            />
            
            <div className="pp__hero_text">
                <InnerBlocks 
                  template = { PP_TEMPLATE }
                  allowedBlocks={["core/paragraph","mailchimp-for-wp/form"]} 
                />
            </div>
            <picture className="pp__hero_image">
                <source media="(max-width: 1023px)" srcset={props.attributes.mobileImgURL} alt="Pura Prote"/>
                <source media="(min-width: 1024px)" srcset={props.attributes.imgURL} alt="Pura Prote"/>
                <img src={props.attributes.imgURL} alt="Pura Prote"/>
            </picture>

        </div>  

        <RichText 
          tagName="p" 
          className={`pp__cintillo hasmedium-font-size`} 
          value={props.attributes.cintillo} 
          onChange={handleCintilloChange} 
          placeholder={ 'Cintillo...' } 
        />
      </>
    )
  }
  
  function SaveComponent() {
    return <InnerBlocks.Content /> 
    // InnerBlocks.content in case it has nested blocks, otherwise it could be null
  }