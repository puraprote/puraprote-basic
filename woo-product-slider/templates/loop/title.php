<?php
/**
 * Product title.
 *
 * This template can be overridden by copying it to yourtheme/woo-product-slider/templates/loop/title.php
 *
 * @package    woo-product-slider
 * @subpackage woo-product-slider/Frontend
 */

if ( $product_name ) {
	?>
	<h3 class="wpsf-product-title"><a href="<?php echo esc_url( get_the_permalink() ); ?>"><?php the_title(); ?></a></h3>
	<?php
}
