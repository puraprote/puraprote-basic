<?php
$style = '';
if ( isset($attributes['cintilloBackgroundColor'])) {
  $style = ' style="background-color: '.$attributes['cintilloBackgroundColor'].'"';
}

$cintillo_class="pp__cintillo wp-block-group alignfull has-global-padding";

if ( isset($attributes['cintilloImages'])) {
  
  $cintillo_images = $attributes['cintilloImages'];


  $cintillo_images_1 = [];
  $cintillo_images_2 = [];

  foreach ($cintillo_images as $key => $value) {
    if ($key < count($cintillo_images) / 2) {
      $cintillo_images_1[] = $value;
    } else {
      $cintillo_images_2[] = $value;
    }
  }



  $cintillo_class .= ' pp__cintillo--has-icons';

}

function pp_cintillo_show_images($image_ids = array()) {
  if ( empty($image_ids) )
    return false;

  echo '<div class="pp__cintillo-icons">';

  foreach ($image_ids as $id) {
    $image = wp_get_attachment_image_src($id, 'full', false);

    echo '<div class="pp__cintillo-icon"><img src="'.$image[0].'" width="'.$image[1].'" height="'.$image[2].'" alt="" /></div>';

  }
  echo '</div>';
}


if ( isset($attributes['cintillo'])) {
  echo '<div class="'.$cintillo_class.'"'.$style.'>';

  // if ( isset($cintillo_images) ) {
  //   pp_cintillo_show_images($cintillo_images_1);
  // }

  echo '<p class="pp__cintillo-text">'.$attributes['cintillo'].'</p>';

  if ( isset($cintillo_images) ) {
    pp_cintillo_show_images($cintillo_images_1);
    pp_cintillo_show_images($cintillo_images_2);

  }

  echo '</div>';
} 

