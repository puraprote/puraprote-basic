/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!*********************************!*\
  !*** ./src/block-variations.js ***!
  \*********************************/
wp.blocks.registerBlockVariation('core/search', {
  name: 'pp-navbar-search',
  title: 'PP Navbar Search',
  attributes: {
    className: 'pp__search'
  }
});
wp.blocks.registerBlockVariation('core/details', {
  name: 'pp-details-dropdown',
  title: 'PP Dropdown',
  attributes: {
    namespace: 'pp-details-dropdown',
    className: 'pp__details pp__details--dropdown pp__box'
  },
  isActive: ['namespace'],
  innerBlocks: [['core/group', {}, [['core/paragraph', {
    placeholder: 'Content here...'
  }]]]]
});
wp.blocks.registerBlockVariation('core/details', {
  name: 'pp-details-accordion',
  title: 'PP Acordeon',
  attributes: {
    namespace: 'pp-details-accordion',
    className: 'pp__details pp__details--accordion'
  },
  isActive: ['namespace'],
  innerBlocks: [['core/group', {}, [['core/paragraph', {
    placeholder: 'Enter content here...'
  }]]]]
});
const VARIATION_NAME = 'namespace/random-ordered-posts';
wp.blocks.registerBlockVariation('core/query', {
  name: 'pp-exclude-noticias',
  title: 'PP Posts excluir noticias',
  description: 'Query loop excluyendo categoria de noticias.',
  icon: 'randomize',
  attributes: {
    namespace: 'pp-exclude-noticias',
    // className: 'random',
    align: 'wide',
    query: {
      perPage: 3,
      postType: 'post',
      inherit: false,
      exclude: []
      // sortByRandom: true // Custom query param
    },
    displayLayout: {
      type: 'flex',
      columns: 3
    }
  },
  isActive: ['namespace'],
  scope: ['inserter'],
  allowedControls: ['layout', 'align'],
  innerBlocks: [['core/post-template', {
    layout: {
      type: 'grid'
    }
  }, [['core/group', {}, [['core/post-featured-image', {
    isLink: true
  }], ['core/post-title', {
    isLink: true,
    fontSize: "medium"
  }], ['core/post-date', {
    fontSize: "small"
  }], ['core/post-author-name', {
    fontSize: "small"
  }], ['core/post-excerpt', {
    moreText: "Leer más"
  }]]]]], ['core/query-pagination'], ['core/query-no-results']]
});
wp.blocks.registerBlockVariation('core/group', {
  name: 'pp-img-row',
  title: 'PP Image row',
  attributes: {
    namespace: 'pp-img-row',
    className: 'pp__img-row',
    layout: {
      type: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'center'
    },
    isActive: ['namespace'],
    allowedBlocks: {
      type: 'array',
      default: ['core/image']
    }
  },
  innerBlocks: [['core/image'], ['core/image'], ['core/image']]
});
/******/ })()
;
//# sourceMappingURL=block-variations.js.map