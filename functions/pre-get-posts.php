<?php

add_action('pre_get_posts', 'pp_search_query_pre');

function pp_search_query_pre($query) {
    if ($query->is_search() && $query->is_main_query()) {
        $tax_query = $query->get('tax_query', array());

        $tax_query[] = array(
            'taxonomy' => 'product_visibility',
            'field'    => 'name',
            'terms'    => 'exclude-from-catalog',
            'operator' => 'NOT IN',
        );

        $query->set('tax_query', $tax_query);
    }
}