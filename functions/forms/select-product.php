<?php
/******************************************
* SHOW ALL ROOMS IN FIELD WITH KEY "ROOMS"
******************************************/
add_filter( 'ninja_forms_render_options', function($options,$settings){
   if( $settings['key'] == 'producto_1694732967447' || $settings['key'] == 'producto_1703378680691' ){
       $args = array(
            'post_type' => 'product',
            'orderby' => 'title',
            'order' => 'ASC',
            'posts_per_page' => 100,
            'post_status' => 'publish',
            'product_cat' => get_queried_object()->slug,
            // 'tax_query'   => array( array(
            //     'taxonomy'  => 'product_visibility',
            //     'terms'     => array( 'exclude-from-catalog' ),
            //     'field'     => 'name',
            //     'operator'  => 'NOT IN',
            // ) )
        );
        // echo '<pre>';
        // var_dump($args);
        // echo '<pre>';

        if ($settings['key'] == 'producto_1694732967447') {
            $tax_query = array(
                array(
                    'taxonomy'  => 'product_visibility',
                    'terms'     => array( 'exclude-from-catalog' ),
                    'field'     => 'name',
                    'operator'  => 'NOT IN',
                )
            );
        } 
        
        if ( $settings['key'] == 'producto_1703378680691' ) {
            $tax_query = array(
                'relation' => 'AND',
                array(
                    'taxonomy'  => 'product_visibility',
                    'terms'     => array( 'exclude-from-catalog' ),
                    'field'     => 'name',
                    'operator'  => 'NOT IN',
                ),
                array(
                    'taxonomy' => 'product_cat',
                    'field' => 'slug',
                    'terms' => array( 'accesorios' ),
                    'operator'  => 'NOT IN',
                )
            );
        }
        $args['tax_query'] = $tax_query;

        // echo '<pre>';
        // var_dump($args);
        // echo '<pre>';
        
       $the_query = new WP_Query( $args ); 
       if ( $the_query->have_posts() ){
           global $post;
           while ( $the_query->have_posts() ){
               $the_query->the_post();
               $options[] = array('label' => get_the_title( ), 'value' => get_the_title( ));
           }
           wp_reset_postdata(); 
       }
   }
   return $options;
},10,2);