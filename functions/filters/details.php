<?php

/*
 * Alter details block markup so summary is a heading
 */

 add_filter('render_block', function ($blockContent, $block) {

    if ($block['blockName'] !== 'core/details') {
        return $blockContent;
    }     
    
    $h = 'h2';

    $details_pattern = '/<details[^>]*class="([^>"]*)"[^>]*>/i';
    preg_match($details_pattern, $blockContent, $matches);
    // print $matches[1];

    if (isset($matches[1])) {
        $heading_marker_pattern = '/pp__details__(h[1-7]{1})/i';
        preg_match($heading_marker_pattern, $matches[1], $heading_class);

        if (isset($heading_class[1])) {
            $h = $heading_class[1];
        }
    }
    

    $pattern = '/(<summary[^>]*>)(.*)(<\/summary>)/i';
    $replacement = '$1<'.$h.'>$2</'.$h.'>$3';
    return preg_replace($pattern, $replacement, $blockContent);

}, 10, 2);