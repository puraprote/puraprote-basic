<?php

/*
 * Alter details block markup so summary is a heading
 * Could be sling-block/pane
 * src https://wordpress.stackexchange.com/questions/376440/gutenberg-blocks-change-edit-part-of-the-block-using-editor-blockedit-filters
 */

 add_filter('render_block', function ($blockContent, $block) {

    if ($block['blockName'] !== 'sling-block/accordion' || get_the_title() !== 'Envíos' ) {
        return $blockContent;
    }     

    // TODO: change to regular expression?

    $find = array(
        '<h2 class="bwf-accordion-head-tag">',
        '</h2>'
    );
    $replace = array(
        '<h3 class="bwf-accordion-head-tag">',
        '</h3>'
    );

    $blockContent = str_replace($find,$replace,$blockContent);

    return $blockContent;

}, 10, 2);