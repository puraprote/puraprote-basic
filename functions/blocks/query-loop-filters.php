<?php

// Filters the front-end output of the query block
function pp_exclude_category_block_variation( $pre_render, $block ) {

    // if ( isset($block['attrs']['namespace']) && 'pp-exclude-noticias' === $block[ 'attrs' ][ 'namespace' ] ) {
    // jsconsole($block[ 'attrs' ][ 'namespace' ]);
    // }

	// Verify it's the block that should be modified using the namespace
	if ( !empty($block['attrs']['namespace']) && 'pp-exclude-noticias' === $block[ 'attrs' ][ 'namespace' ] ) {
        // jsconsole('inside pp exclude noticias');
		add_filter( 'query_loop_block_query_vars', function( $query ) {

            $category = get_category_by_slug('noticias');

			

		
			$post = get_queried_object();

			// also likely want to set order by this key in ASC so next event listed first
			// $query['orderby'] = 'rand';
			// $query['category__not_in'] = [ $category->term_id ];
            $query['cat'] = '-'.$category->term_id;

            // echo '<pre>';
            // var_dump($query);
            // echo '</pre>';

			return $query;
		}, 10, 2 );
	}

	return $pre_render;
}
add_filter( 'pre_render_block', 'pp_exclude_category_block_variation', 10, 2 );
