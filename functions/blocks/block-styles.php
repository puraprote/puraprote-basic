<?php

function register_button_block_styles_1() {
    register_block_style(
        'pp-theme/pp-button', // name of your block
        array(
            'name'  => 'pp-box__icon-left', // part of the class that gets added to the block.
            'label' => __( 'Icon Left', 'style-1' ),
        )
    );
    register_block_style(
        'pp-theme/pp-button', // name of your block
        array(
            'name'  => 'pp-box__icon-right', // part of the class that gets added to the block.
            'label' => __( 'Icon RIght', 'style-1' ),
        )
    );

    register_block_style(
        'core/details', // name of your block
        array(
            'name'  => 'pp-details-dropdown__not-all-caps', // part of the class that gets added to the block.
            'label' => __( 'No mayúsculas', 'style-1' ),
        )
    );

    register_block_style(
        'core/details', // name of your block
        array(
            'name'  => 'pp-details__green', // part of the class that gets added to the block.
            'label' => __( 'Verde', 'style-1' ),
        )
    );
    register_block_style(
        'core/details', // name of your block
        array(
            'name'  => 'pp-detail__orange', // part of the class that gets added to the block.
            'label' => __( 'Naranja', 'style-1' ),
        )
    );
    register_block_style(
        'core/details', // name of your block
        array(
            'name'  => 'pp-details__purple', // part of the class that gets added to the block.
            'label' => __( 'Morado', 'style-1' ),
        )
    );
    register_block_style(
        'core/details', // name of your block
        array(
            'name'  => 'pp-details__red', // part of the class that gets added to the block.
            'label' => __( 'Rojo', 'style-1' ),
        )
    );

    register_block_style(
        'core/columns', // name of your block
        array(
            'name'  => 'pp-columns__wp-breakpoints', // part of the class that gets added to the block.
            'label' => __( 'WP breakpoints', 'style-1' ),
        )
    );

    register_block_style(
        'core/group', // name of your block
        array(
            'name'  => 'pp__topbar--group', // part of the class that gets added to the block.
            'label' => __( 'PP Topbar', 'style-1' ),
        )
    );

    register_block_style(
        'core/post-template', // name of your block
        array(
            'name'  => 'pp__shop-grid', // part of the class that gets added to the block.
            'label' => __( 'PP Shop Grid', 'style-1' ),
        )
    );

    // register_block_style(
    // 	'core/details', // name of your block
    // 	array(
    // 		'name'  => 'pp__dropdown', // part of the class that gets added to the block.
    // 		'label' => __( 'PP Details', 'style-1' ),
    // 	)
    // );
    // sling-block/accordion
}
add_action( 'init', 'register_button_block_styles_1' );