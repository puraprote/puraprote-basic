<?php



namespace PP\ProductSummary;

add_filter(
    'register_block_type_args',
    __NAMESPACE__ . '\change_render_callback_for_query_pagination_numbers',
    10,
    2
);

function change_render_callback_for_query_pagination_numbers( array $settings, string $name ): array {
    if ( 'core/post-excerpt' === $name ) {
        $settings['render_callback'] = __NAMESPACE__ . '\render_query_pagination_numbers';
    }
    return $settings;
}

function render_query_pagination_numbers( array $attributes, string $content, \WP_Block $block ): string {


    $content = wpautop(get_the_excerpt());

    // $content = '<h1>HELLO WORLD</h1>';

    return $content;
}