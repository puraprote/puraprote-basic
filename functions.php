<?php
/**
 * Functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package puraprote
 * @since 1.0.0
 */

/**
 * Add theme support.
 */
function puraprote_setup() {
	/*
	 * Load additional block styles.
	 */
	$styled_blocks = [ 'quote' ];
	foreach ( $styled_blocks as $block_name ) {
		$args = array(
			'handle' => "puraprote-$block_name",
			'src'    => get_theme_file_uri( "assets/css/blocks/$block_name.css" ),
			'path'   => get_theme_file_path( "assets/css/blocks/$block_name.css" ),
		);
		// Replace the "core" prefix if you are styling blocks from plugins.
		// wp_enqueue_block_style( "core/$block_name", $args );
	}

	add_theme_support('editor_styles');
	add_editor_style('build/style-index.css');

	add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'puraprote_setup' );

/**
 * Enqueue the CSS files.
 *
 * @since 1.0.0
 *
 * @return void
 */
function puraprote_styles() {
	wp_enqueue_style(
		'puraprote-style',
		get_stylesheet_uri(),
		[],
		wp_get_theme()->get( 'Version' )
	);

	wp_enqueue_style(
		'pp-styles', 
		get_theme_file_uri('/build/style-index.css')
	);
	/*
     * Google Fonts
     *
     * font-family: 'Fira Sans', sans-serif;  400 600
     */
    wp_enqueue_style( 'google-font', 'https://fonts.googleapis.com/css2?family=Fira+Sans:ital,wght@0,400;0,600;1,400;1,600&display=swap', false );

	wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');

	wp_enqueue_script(
		'pp-details-animation',
		get_template_directory_uri() . '/build/details-animation.js',
		array()
	);

	wp_enqueue_script(
		'pp-mobile-submenus',
		get_template_directory_uri() . '/build/mobile-submenus.js',
		array()
	);

	wp_enqueue_script(
		'pp-animate-text',
		get_template_directory_uri() . '/build/animate-text.js',
		array('jquery')
	);

	wp_enqueue_script(
		'pp-glide',
		'https://cdnjs.cloudflare.com/ajax/libs/Glide.js/3.0.2/glide.js',
		// get_template_directory_uri() . '/node_modules/@glidejs/glide/dist/glide.min.js',
		array()
	);
}
add_action( 'wp_enqueue_scripts', 'puraprote_styles' );

function pp_editor_assets() {
	wp_enqueue_script(
		'pp-block-variations',
		get_template_directory_uri() . '/build/block-variations.js',
		array( 'wp-blocks' )
	);

}
add_action( 'enqueue_block_editor_assets', 'pp_editor_assets' );



/* Initialize Glide */
add_action('wp_footer', 'pp_init_glide');
function pp_init_glide(){
?>
<script>
var sliders = document.querySelectorAll('.glide');

for (var i = 0; i < sliders.length; i++) {
  var glide = new Glide(sliders[i], {
	type: 'carousel',
  animationDuration: 2000,
  autoplay: 7500,
  focusAt: '1',
  startAt: 1,
  perView: 1, 
  });

  glide.mount()
}
</script>
<?php
};



// Register Navigation Menus
// function pp_navigation_menus() {

// 	$locations = array(
// 		'header_countries' => __( 'Menú de tiendas por país', 'puraprote' ),
// 		'header_pagelinks' => __( 'Menú con las diferentes páginas y secciones', 'puraprote' ),
// 		'header_utilities' => __( 'Menú utlidades (búsqueda, carrito)', 'puraprote' ),
// 		'header_userlinks' => __( 'Menú enlaces del usuario', 'puraprote' ),
// 		'footer_links' => __( 'Enlaces en el footer', 'puraprote' ),
// 	);
// 	register_nav_menus( $locations );

// }
// add_action( 'init', 'pp_navigation_menus' );


class PP_Block {
	function __construct($name, $renderCallback = null, $data = null) {
		$this->name = $name;
		$this->data = $data;
		$this->renderCallback = $renderCallback;
		add_action('init', [$this, 'onInit']);
	  }

	// We need to access attributes AND the nested content
	// This will return the HTML that is actually used on the frontend
	function ourRenderCallback($attributes, $content) {
		ob_start();
		require get_theme_file_path("/blocks/{$this->name}.php");
		return ob_get_clean();
	}

	function onInit() {
		wp_register_script($this->name,get_stylesheet_directory_uri()."/build/{$this->name}.js",array('wp-blocks','wp-editor'));

		if ($this->data) {
			$scriptName = str_replace('-','_',$this->name);
			wp_localize_script($scriptName, $scriptName, $this->data);
		  }

	  $ourArgs = array(
		'editor_script' => $this->name
	  );
  
  
	  if ($this->renderCallback) {
		//HAS to be 'render_callback'
		$ourArgs['render_callback'] = [$this, 'ourRenderCallback'];
	  }

	  register_block_type("pp-theme/{$this->name}", $ourArgs);
	}
}
  
new PP_Block('pp-topbar');
new PP_Block('pp-hero-home', true);
new PP_Block('pp-heading', true);
new PP_Block('pp-heading-plain', true);
new PP_Block('pp-paragraph-plain', true);
new PP_Block('pp-button', true);
new PP_Block('pp-hero', true);
new PP_Block('pp-test', true);
new PP_Block('pp-dropdown', true);
new PP_Block('pp-text-animation', true);
// new PP_Block('pp-typewriter', true);
new PP_Block('pp-grid', true);
new PP_Block('pp-logos', true);






	// Get margin and padding style from editor
	function pp_get_spacing_values($attributes,$property_to_get = null) {
		if (!$attributes)
		    return false;

		$style = '';
	
		foreach ($attributes as $prop => $values) {
			// echo '<pre>';
			// var_dump($attributes);
			// echo '</pre>';
			if ($property_to_get == null || $property_to_get == $prop ) {
				if (is_array($values)) {
					foreach ($values as $prop2 => $value) {
						if ( str_contains($value,'var:')) {
							$value = str_replace('|','--',$value);
							$value = str_replace('var:','var(--wp--',$value);
							$value .= ')';
						}
						
						$style .= $prop.'-'.$prop2.': '.$value.';';
					}
				}
			}



		}

		return $style;
	}

// Add line-breaks to excerpt
require_once(get_template_directory().'/functions/blocks/linebreak-excerpt.php');

// QUery loop filters
require_once(get_template_directory().'/functions/blocks/query-loop-filters.php');

// Dropdown filters
// require_once(get_template_directory().'/functions/blocks/dropdown-filters.php');

// Block styles
require_once(get_template_directory().'/functions/blocks/block-styles.php');

// Select field with products
require_once(get_template_directory().'/functions/forms/select-product.php');

//Pre get posts
require_once(get_template_directory().'/functions/pre-get-posts.php');

//Filters
require_once(get_template_directory().'/functions/filters/details.php');
require_once(get_template_directory().'/functions/filters/sling-block_accordion.php');

//Woocommerce
require_once(get_template_directory().'/functions/woocommerce/woocommerce.php');

// SHow all products in admin
add_action('init', 'pp_reset_screen_options');
function pp_reset_screen_options() {
    update_user_meta( get_current_user_id(), 'edit_product_per_page', 50 );
}


function jsconsole($data) {
    $output = $data;
    if (is_array($output))
        $output = implode(',', $output);

    echo "<script>console.log('Debug Objects: " . $output . "' );</script>";
}

